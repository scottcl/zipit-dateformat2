﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zipit_DateFormat2
{
    class Program
    {
        public static void Main(string[] args)
        {
            String str = "Date: February 13, 2016-February 14, 2016, 12:00-12:00AM; (Pre Conferences on February 12, 2016)";
            Console.WriteLine("Input: " + str);
            Console.WriteLine("Output:");
            List<DateTime> myDates = new List<DateTime>();
            myDates = DateFormatter(str);
            if(myDates.Count == 1 && myDates.ElementAt(0) == new DateTime())
            {
                Console.WriteLine("Error, could not convert");
            }
            for (int i = 0; i < myDates.Count; i++)
            {
                Console.WriteLine(myDates.ElementAt(i));
            }
            
            Console.Read();
        }


        public static List<DateTime> DateFormatter(String str)
        {
            List<DateTime> dates = new List<DateTime>();
            DateTime newDate = new DateTime();
            try
            {
                newDate = Convert.ToDateTime(str);
                dates.Add(newDate);
            }
            catch (FormatException ex)
            {
                try
                {
                    newDate = DateTime.Parse(str);
                    dates.Add(newDate);
                }
                catch (FormatException ex2)
                {
                    //Remove . between am and pm
                    str = str.Replace(".", "");
                    //Remove . between am and pm
                    str = str.Replace("AM", " AM ");
                    str = str.Replace("PM", " PM ");
                    //switch from the word midnight to 12am
                    str = str.Replace("midnight", "12am");
                    //switch from the word noon to 12pm
                    str = str.Replace("noon", "12pm");
                    str = str.Replace("Date:", " ");
                    //remove anything after colon in npr format
                    if(str.IndexOf(";") != -1)
                    str = str.Remove(str.IndexOf(";"));
                    try
                    {
                    //remove dash before day of week
                    int dayOfWeek1 = 0;
                    int dayOfWeekLength = 0;
                    if (str.IndexOf("Monday") != -1)
                    {
                        dayOfWeek1 = str.IndexOf("Monday");
                        dayOfWeekLength = 6;
                    }
                    else if(str.IndexOf("Tuesday") != -1)
                    {
                        dayOfWeek1 = str.IndexOf("Tuesday");
                        dayOfWeekLength = 7;
                    }
                    else if (str.IndexOf("Wednesday") != -1)
                    {
                        dayOfWeek1 = str.IndexOf("Wednesday");
                        dayOfWeekLength = 9;
                    }
                    else if (str.IndexOf("Thursday") != -1)
                    {
                        dayOfWeek1 = str.IndexOf("Thursday");
                        dayOfWeekLength = 8;
                    }
                    else if (str.IndexOf("Friday") != -1)
                    {
                        dayOfWeek1 = str.IndexOf("Friday");
                        dayOfWeekLength = 6;
                    }
                    else if (str.IndexOf("Saturday") != -1)
                    {
                        dayOfWeek1 = str.IndexOf("Saturday");
                        dayOfWeekLength = 8;
                    }
                    else if (str.IndexOf("Sunday") != -1)
                    {
                        dayOfWeek1 = str.IndexOf("Sunday");
                        dayOfWeekLength = 6;
                    }
                    str = str.Remove(dayOfWeek1 - 1, 1);
                    //Index of the dash between the times
                    int timeDash = str.IndexOf("-", dayOfWeek1 + dayOfWeekLength);
                    //string of the end time
                    string endTime = str.Substring(timeDash + 1);
                    //string of date and day of week
                    string day = str.Substring(0, dayOfWeek1 + dayOfWeekLength);
                    //length of date and day of week
                    int dayLength = str.Substring(0, dayOfWeek1 + dayOfWeekLength).Length - 1;
                    //Console.WriteLine(endTime.Length+" | "+ ((str.Length - 1) - (endTime.Length - 1)));
                    //length of the start time
                    int startTimeLength = str.Substring(dayLength, (str.Length - 1) - (endTime.Length - 1) - dayLength).Length - 2;
                    //string of the start time
                    string startTime = str.Substring(dayOfWeek1 + dayOfWeekLength, startTimeLength);
                    //determine if am or pm 
                    string AMPM = "";
                    if (str.IndexOf("am", timeDash) != -1)
                    {
                        AMPM = "am";
                    }
                    else
                    {
                        AMPM = "pm";
                    }
                    string start = day + startTime;
                    if(!start.Contains("am") && !start.Contains("pm") && !start.Contains("AM") && !start.Contains("PM"))
                    {
                        start += AMPM;
                    }

                        newDate = Convert.ToDateTime(start);
                        dates.Add(newDate);
                        string end = day + endTime;
                        newDate = Convert.ToDateTime(end);
                        dates.Add(newDate);
                    }catch(Exception ex3)
                    {
                        //test for npr format
                        try {
                            //Index of the dash between the times
                            int timeDash = str.IndexOf("-");
                            //string of the end time
                            int commaIndex = str.IndexOf(",");
                            commaIndex = str.IndexOf(",", commaIndex + 1) + 1;

                            string startTime = str.Substring(commaIndex, timeDash - commaIndex);
                            string endTime = " " + str.Substring(timeDash + 1);
                            //Console.WriteLine(endTime);
                            string day = str.Substring(0, commaIndex - 1);

                            string start = day + startTime;
                            newDate = Convert.ToDateTime(start);
                            dates.Add(newDate);
                            string end = day + endTime;
                            newDate = Convert.ToDateTime(end);
                            dates.Add(newDate);
                        }catch(Exception ex4)
                        {
                            //test for npr format with multiple dates
                            try
                            {
                                //Index of the dash between the days
                                int dateDash = str.IndexOf("-");
                                string startDay = str.Substring(0, dateDash);
                                int onIndex = str.IndexOf(" on");
                                //Console.WriteLine(startDay);
                                string endDay = str.Substring(dateDash + 1, onIndex - dateDash);
                               // Console.WriteLine(endDay);
                                string dayOfWeek = "";
                                int dayOfWeekIndex = 0;
                                DayOfWeek DOW = DayOfWeek.Monday;
                                if (str.IndexOf("Monday") != -1)
                                {
                                    dayOfWeekIndex = str.IndexOf("Monday");
                                    dayOfWeek = "Monday";
                                    DOW = DayOfWeek.Monday;
                                }
                                else if (str.IndexOf("Tuesday") != -1)
                                {
                                    dayOfWeekIndex = str.IndexOf("Tuesday");
                                    dayOfWeek = "Tuesday";
                                    DOW = DayOfWeek.Tuesday;
                                }
                                else if (str.IndexOf("Wednesday") != -1)
                                {
                                    dayOfWeekIndex = str.IndexOf("Wednesday");
                                    dayOfWeek = "Wednesday";
                                    DOW = DayOfWeek.Wednesday;
                                }
                                else if (str.IndexOf("Thursday") != -1)
                                {
                                    dayOfWeekIndex = str.IndexOf("Thursday");
                                    dayOfWeek = "Thursday";
                                    DOW = DayOfWeek.Thursday;
                                }
                                else if (str.IndexOf("Friday") != -1)
                                {
                                    dayOfWeekIndex = str.IndexOf("Friday");
                                    dayOfWeek = "Friday";
                                    DOW = DayOfWeek.Friday;
                                }
                                else if (str.IndexOf("Saturday") != -1)
                                {
                                    dayOfWeekIndex = str.IndexOf("Saturday");
                                    dayOfWeek = "Saturday";
                                    DOW = DayOfWeek.Saturday;
                                }
                                else if (str.IndexOf("Sunday") != -1)
                                {
                                    dayOfWeekIndex = str.IndexOf("Sunday");
                                    dayOfWeek = "Sunday";
                                    DOW = DayOfWeek.Sunday;
                                }

                                int timeDash = str.IndexOf("-", dayOfWeekIndex);
                                string startTime = str.Substring(dayOfWeekIndex + dayOfWeek.Length, timeDash - (dayOfWeekIndex + dayOfWeek.Length));
                                string endTime = str.Substring(timeDash + 1);
                                
                                string startDate = startDay + startTime;
                                string endDate = endDay + startTime;
                                DateTime startDateTime = DateTime.Parse(startDate);
                                DateTime endDateTime = DateTime.Parse(endDate);

                                DateTime startDateStart = DateTime.Parse(startDay + startTime);
                                DateTime startDateEnd = DateTime.Parse(startDay + endTime);
                                if(startDateEnd.Hour == 12 || startDateEnd.Hour == 0)
                                {
                                    startDateEnd = startDateEnd.AddDays(1);
                                }
                                //start is less than end
                                while (DateTime.Compare(startDateStart, endDateTime) < 0)
                                {
                                   // Console.WriteLine(startDateStart+" | "+endDateTime+DateTime.Compare(startDateStart, endDateTime));
                                  // Console.Read();
                                    if (startDateStart.DayOfWeek == DOW)
                                    {
                                        
                                        dates.Add(startDateStart);
                                        dates.Add(startDateEnd);
                                        
                                    }
                                    startDateStart = startDateStart.AddDays(1);
                                    startDateEnd = startDateEnd.AddDays(1);
                                }
                                
                            }
                            catch (Exception ex5)
                            {
                                //check npr format without year
                                try {
                                    CultureInfo provider = CultureInfo.InvariantCulture;
                                    string pattern = "MMMM dd, h:mm tt ";
                                    str = str.Trim();
                                    int commaIndex = str.IndexOf(",");
                                    int timeDash = str.IndexOf("-", commaIndex);
                                    string startTime = str.Substring(commaIndex + 1, timeDash - commaIndex - 1);
                                    string endTime = " " + str.Substring(timeDash + 1) + " ";
                                    string day = str.Substring(0, commaIndex + 1);
                                    DateTime endDate = DateTime.ParseExact(day + endTime + "", pattern, provider);
                                    if(endDate.Hour == 12 || endDate.Hour == 0)
                                    {
                                        endDate = endDate.AddDays(1);
                                    }
                                    // Console.WriteLine(day + endTime + "");
                                    dates.Add(DateTime.ParseExact(day + startTime, pattern, provider));
                                    dates.Add(endDate);
                                }catch(Exception ex6)
                                {
                                    //check npr format without year and multiple dates same month
                                    try
                                    {
                                        CultureInfo provider = CultureInfo.InvariantCulture;
                                        string pattern = "MMMM dd, h:mm tt ";
                                        str = str.Trim();
                                        int commaIndex = str.IndexOf(",");
                                        int timeDash = str.IndexOf("-", commaIndex);
                                        string startTime = str.Substring(commaIndex + 1, timeDash - commaIndex - 1);
                                        string endTime = " " + str.Substring(timeDash + 1) + " ";
                                        int dateDash = str.IndexOf("-");
                                        string endDay = str.Substring(dateDash + 1, commaIndex - dateDash);

                                        string startDay = str.Substring(0, dateDash);
                                        DateTime startDate = DateTime.ParseExact(startDay + "," + startTime, pattern, provider);
                                        DateTime startDateEndTime = DateTime.ParseExact(startDay + "," + endTime, pattern, provider);
                                        if (startDateEndTime.Hour == 12 || startDateEndTime.Hour == 0)
                                        {
                                            startDateEndTime = startDateEndTime.AddDays(1);
                                        }
                                        int month = startDate.Month;
                                        pattern = "M dd, h:mm tt ";
                                        // Console.WriteLine(month + " " + endDay + endTime+"|");
                                        DateTime endDate = DateTime.ParseExact(month + " " + endDay + startTime, pattern, provider);
                                        while (DateTime.Compare(startDate, endDate) <= 0)
                                        {
                                            dates.Add(startDate);
                                            dates.Add(startDateEndTime);
                                            startDate = startDate.AddDays(1);
                                            startDateEndTime = startDateEndTime.AddDays(1);
                                        }
                                    }
                                    catch (Exception ex7)
                                    {
                                        try
                                        {
                                            CultureInfo provider = CultureInfo.InvariantCulture;
                                            string pattern = "MMMM dd, h:mm tt ";
                                            str = str.Trim();
                                            int dateDash = str.IndexOf("-");
                                            string startDay = str.Substring(0, dateDash);
                                            int commaIndex = str.IndexOf(",", dateDash);
                                            commaIndex = str.IndexOf(",", commaIndex + 1);
                                            string endDay = str.Substring(dateDash + 1, commaIndex - dateDash - 1);
                                            int timeDash = str.IndexOf("-", commaIndex);

                                            string startTime = str.Substring(commaIndex + 1, timeDash - commaIndex - 1);
                                            startTime = startTime.Trim();
                                            string endTime = str.Substring(timeDash + 1);
                                            if (!startTime.Contains("AM") && !startTime.Contains("PM"))
                                            {
                                                if (endTime.Contains("AM"))
                                                {
                                                    startTime += " AM";
                                                    if (startTime == endTime)
                                                    {
                                                        startTime = startTime.Replace("AM", "PM");
                                                    }
                                                }
                                                else
                                                {
                                                    startTime += " PM";
                                                }
                                            }
                                           // Console.WriteLine(startDay + " " + startTime);
                                            DateTime startDate = DateTime.Parse(startDay + " " + startTime);
                                            DateTime startDateEndTime = DateTime.Parse(startDay + " " + endTime);
                                            if (startDateEndTime.Hour == 12 || startDateEndTime.Hour == 0)
                                            {
                                                startDateEndTime = startDateEndTime.AddDays(1);
                                            }
                                            DateTime endDate = DateTime.Parse(endDay + " " + startTime);
                                            //Console.WriteLine("444 "+ endDate);
                                            while(DateTime.Compare(startDate, endDate) <= 0)
                                            {
                                                dates.Add(startDate);
                                                dates.Add(startDateEndTime);
                                                startDate = startDate.AddDays(1);
                                                startDateEndTime = startDateEndTime.AddDays(1);
                                            }

                                        }
                                        catch (Exception ex8)
                                        {

                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            if(dates.Count == 0)
            {
                dates.Add(new DateTime());
            }
            return dates;
        }

        public static String FindStartDate(string str)
        {
            string pattern = @"\d{4}";
            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(pattern);
            int yearIndex = str.IndexOf(r.Match(str).ToString()) + 4;
            string date = str.Substring(0, yearIndex);
            return date;
        }

        public static String FindFirstDay(string str)
        {
            string pattern = @"\d{4}";
            System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(pattern);
            int yearIndex = str.IndexOf(r.Match(str).ToString()) + 4;
            int spaceIndex =  str.IndexOf(" ") - yearIndex;
            string day = str.Substring(yearIndex + 1, spaceIndex);
            return day;
        }

        public static List<String> FindFirstDayTime(string str)
        {
            List<String> times = new List<String>();
            int AMPMIndex = str.IndexOf("a.m.");
            if(AMPMIndex == -1)
            {
                str.IndexOf("p.m.");
            }
            int dayIndex = str.IndexOf(FindFirstDay(str)) + FindFirstDay(str).Length;
            string time = str.Substring(dayIndex, AMPMIndex - dayIndex);
            int dashIndex = time.IndexOf("-");
            string startTime = time.Substring(0, (time.Length - 1) - dashIndex);
            string endTime = time.Substring(dashIndex);
            times.Add(startTime);
            times.Add(endTime);
            return times;
        }
        
    }
}
